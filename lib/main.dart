import 'dart:ui';

import 'package:flutter/material.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget personal = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  padding: const EdgeInsets.only(
                    bottom: 8,
                  ),
                  child: Text(
                    'Personal Profile',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.blueAccent[100],
                        fontSize: 25),
                  )),
              Text(
                  'I am Chonthicha Boonjamnian, my nickname is earn, 21 years old who has a strong passion and interest for programmimg.',
                  style: TextStyle(color: Colors.grey[500]))
            ],
          )),
        ],
      ),
    );
    Widget information = Container(
        padding: const EdgeInsets.all(32),
        child: Row(
          children: [
            Expanded(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    padding: const EdgeInsets.only(
                      bottom: 8,
                    ),
                    child: Text(
                      'Contact',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.blueAccent[100],
                          fontSize: 25),
                    )),
                Text('E-mail : 61160137@go.buu.ac.th',
                    style: TextStyle(color: Colors.grey[500])),
                Text('Tel : 098-581-9473',
                    style: TextStyle(color: Colors.grey[500])),
                Text(
                    'Address : 85/1075 Soi pracha-utid79, pracha-utid road, tungkru, tungkru, bangkok, 10140',
                    style: TextStyle(color: Colors.grey[500])),
              ],
            )),
          ],
        ));
    Widget education = Container(
        padding: const EdgeInsets.all(32),
        child: Row(
          children: [
            Expanded(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    padding: const EdgeInsets.only(
                      bottom: 8,
                    ),
                    child: Text(
                      'Education',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.blueAccent[100],
                          fontSize: 25),
                    )),
                Text(
                    'I learn computer science faculty of informatics at burapha university, I am 4 years student.',
                    style: TextStyle(color: Colors.grey[500])),
              ],
            )),
          ],
        ));
    Widget skills = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  padding: const EdgeInsets.only(
                    bottom: 5,
                  ),
                  child: Text(
                    'Skills & competencies',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.blueAccent[100],
                        fontSize: 25),
                  )),
            ],
          )),
        ],
      ),
    );
    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Image.asset(
            'images/html.png',
            width: 60,
            height: 60,
            fit: BoxFit.contain,
          ),
          Image.asset(
            'images/vue.png',
            width: 60,
            height: 60,
            fit: BoxFit.contain,
          ),
          Image.asset(
            'images/java.png',
            width: 60,
            height: 60,
            fit: BoxFit.contain,
          ),
          Image.asset(
            'images/sql.png',
            width: 60,
            height: 60,
            fit: BoxFit.contain,
          ),
          Image.asset(
            'images/mysql.png',
            width: 60,
            height: 60,
            fit: BoxFit.contain,
          ),
        ],
      ),
    );

    return MaterialApp(
        title: ('Resume'),
        home: Scaffold(
          appBar: AppBar(
            title: const Text('Resume'),
          ),
          body: ListView(children: [
            Image.asset(
              'images/me.jpg',
              width: 600,
              height: 300,
              fit: BoxFit.cover,
            ),
            personal,
            information,
            education,
            skills,
            buttonSection
          ]),
        ));
  }
}
